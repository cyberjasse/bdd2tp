<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<HTML><body>
			<xsl:apply-templates select="programme"/>
		</body></HTML>
	</xsl:template>
	<xsl:template match="programme">
		<h2>Programme de BAC3 Info</h2>
		<table BORDER="1">
			<tr>
				<td>Cours</td>
				<td>prof</td>
				<td>ECTS</td>
			</tr>
			<xsl:for-each select="cours[qui/bac/@dept='info'][qui/bac/@annee='3']">
				<tr>
					<td><xsl:value-of select="nom"/></td>
					<td><xsl:value-of select="prof"/></td>
					<td><xsl:value-of select="qui/bac[@dept='info'][@annee='3']/@ects"/></td>
				</tr>
			</xsl:for-each>
		</table>
		Total ECTS: <xsl:value-of select="sum(cours/qui/bac[@dept='info'][@annee='3']/@ects)"/>
		<h2>Programme de BAC3 Math</h2>
		<table BORDER="1">
			<tr>
				<td>Cours</td>
				<td>prof</td>
				<td>ECTS</td>
			</tr>
			<xsl:for-each select="cours[qui/bac/@dept='math'][qui/bac/@annee='3']">
				<tr>
					<td><xsl:value-of select="nom"/></td>
					<td><xsl:value-of select="prof"/></td>
					<td><xsl:value-of select="qui/bac[@dept='math'][@annee='3']/@ects"/></td>
				</tr>
			</xsl:for-each>
		</table>
		Total ECTS: <xsl:value-of select="sum(cours/qui/bac[@dept='math'][@annee='3']/@ects)"/>
		<h2>Programme des preparatoires Info</h2>
		<table BORDER="1">
			<tr>
				<td>Cours</td>
				<td>prof</td>
				<td>ECTS</td>
			</tr>
			<xsl:for-each select="cours[qui/prep/@dept='info']">
				<tr>
					<td><xsl:value-of select="nom"/></td>
					<td><xsl:value-of select="prof"/></td>
					<td><xsl:value-of select="qui/prep[@dept='info']/@ects"/></td>
				</tr>
			</xsl:for-each>
		</table>
		Total ECTS: <xsl:value-of select="sum(cours/qui/prep[@dept='info']/@ects)"/>
	</xsl:template>
</xsl:stylesheet>
