<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<HTML><body>
			<xsl:apply-templates select="personnes/personne" mode="root"/>
		</body></HTML>
	</xsl:template>
	<xsl:template match="personne" mode="root">
		<h1>
			<xsl:value-of select="@nom"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="@prenom"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="position()"/>
		</h1>
		<b>
			<xsl:apply-templates select="email[1]"/>
		</b>
		<xsl:apply-templates select="email[not(position()=1)]"/>
		<br/>Personnes avec le meme nom:
		<br/>
		<xsl:apply-templates select="preceding::personne[@nom=current()/@nom]" mode="same"/>
		<xsl:apply-templates select="following::personne[@nom=current()/@nom]" mode="same"/>
	</xsl:template>
	<xsl:template match="email">
		<br/><a>
			<xsl:attribute name="href">
				<xsl:value-of select="."/>
			</xsl:attribute>
			<xsl:value-of select="."/>
		</a>
	</xsl:template>
	<xsl:template match="personne" mode="same">
		<xsl:value-of select="@prenom"/>
		<xsl:text>, </xsl:text>
	</xsl:template>
</xsl:stylesheet>
