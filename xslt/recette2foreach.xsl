<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/recette">
		<HTML>
			<title>
				<xsl:value-of select="entete/titre"/>
			</title>
			<body>
				<h1>
					<xsl:value-of select="entete/titre"/>
				</h1>
				<xsl:value-of select="entete/auteur"/>
				<br/>
				Remarque: <xsl:value-of select="entete/remarque"/>
				<h2>Procedure</h2>
				<ul>
					<xsl:for-each select="procedure/liste/item">
						<li> <xsl:value-of select="."/> </li>
					</xsl:for-each>
				</ul>
				<xsl:for-each select="procedure/texte">
					<p> <xsl:value-of select="."/> </p>
				</xsl:for-each>
			</body>
		</HTML>
	</xsl:template>
</xsl:stylesheet>
