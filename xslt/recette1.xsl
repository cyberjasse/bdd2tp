<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/recette">
		<HTML>
			<title>
				<xsl:value-of select="entete/titre"/>
			</title>
			<body>
				<xsl:apply-templates select="entete"/>
				<xsl:apply-templates select="procedure"/>
			</body>
		</HTML>
	</xsl:template>
	<xsl:template match="entete">
		<h1>
			<xsl:value-of select="titre"/>
		</h1>
		<xsl:value-of select="auteur"/>
		<br/>
		Remarque: <xsl:value-of select="remarque"/>
	</xsl:template>
	<xsl:template match="procedure">
		<h2>Procedure</h2>
		<p>
			<xsl:value-of select="."/>
		</p>
	</xsl:template>
</xsl:stylesheet>
