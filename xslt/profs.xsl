<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<HTML><body>
			<xsl:apply-templates select="programme"/>
		</body></HTML>
	</xsl:template>
	<xsl:template match="programme">
		<h2>Programme des professeurs</h2>
		<table BORDER="1">
			<xsl:apply-templates select="cours[not(prof=preceding-sibling::/cours/prof)]/prof"/>
		</table>
	</xsl:template>
	<xsl:template match="prof">
		<tr>
			<td><xsl:value-of select="."/></td>
			<td><table>
				<td><xsl:apply-templates select="/programme/cours[prof=current()]/nom"/></td>
			</table></td>
		</tr>
	</xsl:template>
	<xsl:template match="nom">
		<tr><xsl:value-of select="."/></tr>
	</xsl:template>
</xsl:stylesheet>
