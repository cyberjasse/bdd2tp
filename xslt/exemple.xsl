<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<HTML><body><h2>mail</h2>
			<xsl:apply-templates/>
		</body></HTML>
	</xsl:template>
	<xsl:template match="personnes">
		<xsl:apply-templates select="personne"/>
	</xsl:template>
	<xsl:template match="personne">
		<FONT COLOR="red">
			<xsl:apply-templates select="email"/>
		</FONT>
		</br>
	</xsl:template>
	<xsl:template match="email">
		<xsl:value-of select="."/>
	</xsl:template>
</xsl:stylesheet>
