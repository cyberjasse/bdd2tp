<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html><body>
			<ul>
				<xsl:apply-templates select="dict/mot"/>
				<xsl:apply-templates select="dict/prefixe"/>
			</ul>
		</body></html>
	</xsl:template>
	<xsl:template match="prefixe">
		<li>
			<xsl:value-of select="@value"/>
			<xsl:apply-templates select="mot[@value='']"/>
			<ul>
				<xsl:apply-templates select="mot[not(@value='')]"/>
				<xsl:apply-templates select="prefixe"/>
			</ul>
		</li>
	</xsl:template>
	<xsl:template match="mot[@value='']">
		<xsl:text> (mot complet)</xsl:text>
	</xsl:template>
	<xsl:template match="mot">
		<li>
			<xsl:value-of select="@value"/>
		</li>
	</xsl:template>
</xsl:stylesheet>
