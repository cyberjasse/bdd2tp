<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<HTML><body>
			<xsl:apply-templates select="programme"/>
		</body></HTML>
	</xsl:template>
	<xsl:template match="programme">
		<h2>Programme de BAC3 Info</h2>
		<table BORDER="1">
			<tr>
				<td>Cours</td>
				<td>prof</td>
				<td>ECTS</td>
			</tr>
			<xsl:apply-templates select="cours/qui/bac[@dept='info'][@annee='3']"/>
		</table>
		Total ECTS: <xsl:value-of select="sum(cours/qui/bac[@dept='info'][@annee='3']/@ects)"/>
		<h2>Programme de BAC3 Math</h2>
		<table BORDER="1">
			<tr>
				<td>Cours</td>
				<td>prof</td>
				<td>ECTS</td>
			</tr>
			<xsl:apply-templates select="cours/qui/bac[@dept='math'][@annee='3']"/>
		</table>
		<h2>Programme des preparatoires Info</h2>
		<table BORDER="1">
			<tr>
				<td>Cours</td>
				<td>prof</td>
				<td>ECTS</td>
			</tr>
			<xsl:apply-templates select="cours/qui/prep[@dept='info']"/>
		</table>
	</xsl:template>
	<xsl:template match="bac">
		<tr>
			<td><xsl:value-of select="ancestor::cours/nom"/></td>
			<td><xsl:value-of select="ancestor::cours/prof"/></td>
			<td><xsl:value-of select="@ects"/></td>
		</tr>
	</xsl:template>
	<xsl:template match="prep">
		<tr>
			<td><xsl:value-of select="ancestor::cours/nom"/></td>
			<td><xsl:value-of select="ancestor::cours/prof"/></td>
			<td><xsl:value-of select="@ects"/></td>
		</tr>
	</xsl:template>
</xsl:stylesheet>
