<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<HTML><body><h2>6.1 exercice introductif</h2>
			<xsl:apply-templates select="//personne"/>
		</body></HTML>
	</xsl:template>
	<xsl:template match="personne">
		<h1>
			<xsl:value-of select="@nom"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="@prenom"/>
		</h1>
		<font COLOR="red">
			<xsl:apply-templates select="email"/>
			<br/>
			<xsl:apply-templates select="adresse"/>
		</font>
		<br/>
	</xsl:template>
	<xsl:template match="email">
		<xsl:value-of select="."/>
	</xsl:template>
	<xsl:template match="adresse">
		<xsl:value-of select="."/>
		<br/>
		<xsl:value-of select="@code"/>
	</xsl:template>
</xsl:stylesheet>
