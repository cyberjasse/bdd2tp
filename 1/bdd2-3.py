from xml.dom import minidom

racine = minidom.parse('bdd1-1.xml')
doc = racine.getElementsByTagName('person')[0]
print "--- afficher arbre complet"
print doc.toxml()
print "--- afficher nom et prenom"
print doc.getAttribute('nom')
print doc.getAttribute('prenom')
print "--- afficher email"
email = doc.getElementsByTagName('email')[0]
print email.firstChild.data
print "--- afficher adresse et code postale"
adresse = doc.getElementsByTagName('adresse')[0]
print adresse.firstChild.data
print adresse.getAttribute('code')


print "--- tag inexistant"
unexist = doc.getElementsByTagName('hophop')
print unexist
print "--- contenu data"
num = doc.getElementsByTagName('telephone')[0]
print num.getAttribute('data')
print "--- methodes du noeud textuel fils"
print dir(email.firstChild.data)
print "nombre d'element fils du noeud racine"
print len(racine.childNodes)
print "modifier email"
email.firstChild.data = "troll@yahoo.fr"
print racine.getElementsByTagName('person')[0].getElementsByTagName('email')[0].firstChild.data
print "modifier numero telephone"
num.attributes['num'].value = '00128143'
print racine.getElementsByTagName('person')[0].getElementsByTagName('telephone')[0].getAttribute('num')
